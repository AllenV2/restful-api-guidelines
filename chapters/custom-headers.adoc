[[Custom-headers]]
= Custom Headers

This section shares definitions of custom headers that should be
named consistently because they address overarching service-related
concerns. Whether services support these concerns or not is optional;
therefore, the OpenAPI API specification is the right place to make this
explicitly visible. Use the parameter definitions of the resource HTTP
methods.

[#183]
== {MUST} Use Only the Specified Custom InfoArmor Headers

*_to be defined_* As a general rule, custom HTTP headers should be avoided. Still
they can be useful in cases where context needs to be passed through
multiple services in an end-to-end fashion. As such, a valid use-case
for a custom header is providing context information, which is not
a part of the actual API, but is needed by subsequent communication.

From a conceptual point of view, the semantics and intent of an
operation should always be expressed by URLs path and query parameters,
the method, and the content. Headers are more often used to implement
functions close to the protocol considerations, such as flow control,
content negotiation, and authentication. Thus, headers are reserved for
general context information ({RFC-7231}#section-5[RFC 7231]).

`X-` headers were initially reserved for unstandardized parameters, but the
usage of `X-` headers is deprecated ({RFC-6648}[RFC 6648]). This complicates
the contract definition between consumer and producer of an API following
these guidelines, since there is no aligned way of using those headers.
Because of this, the guidelines restrict which `X-` headers can be used
and how they are used.

The Internet Engineering Task Force's states in {RFC-6648}[RFC 6648] that
company specific header' names should incorporate the organization's name.
We aim for backward compatibility, and therefore keep the `X-` prefix.

The following custom headers have been specified by this guideline
for usage so far. Remember that HTTP header field names are not
case-sensitive. *_to be defined_*

[cols="15%,10%,60%,15%",options="header",]
|=======================================================================
|Header field name |Type |Description |Header field value example

|[[x-correlation-id]]{X-Correlation-ID}|String|
For more information see <<233>>.
|GKY7oDhpSiKY_gAAAABZ_A

|[[x-tenant-id]]{X-Tenant-ID}|String|
Identifies the tenant initiated the request
to the multi tenant InfoArmor Platform. The {X-Tenant-ID} must be set
according to the Business Partner ID extracted from the OAuth token when
a request from a Business Partner hits the InfoArmor Platform.
|9f8b3ca3-4be5-436c-a847-9cd55460c495

|[[c-frontend-type]]{X-Frontend-Type}|String|
Consumer facing applications (CFAs) provide business experience to their
customers via different frontend application types, for instance, mobile app
or browser. Info should be passed-through as generic aspect -- there are
diverse concerns, e.g. pushing mobiles with specific coupons, that make use of
it. Current range is mobile-app, browser, facebook-app, chat-app
|mobile-app

|[[x-device-type]]{X-Device-Type}|String|
There are also use cases for steering customer experience (incl. features and
content) depending on device type. Via this header info should be passed-through
as generic aspect. Current range is smartphone, tablet, desktop, other.
|tablet

|[[x-device-os]]{X-Device-OS}|String|
On top of device type above, we even want to differ between device platform,
e.g. smartphone Android vs. iOS. Via this header info should be passed-through
as generic aspect. Current range is iOS, Android, Windows, Linux, MacOS.
|Android
|=======================================================================

*Exception:* The only exception to this guideline are the conventional
hop-by-hop `X-RateLimit-` headers which can be used as defined in <<153>>.

[#184]
== {MUST} Propagate Custom Headers

*_to be defined_* All InfoArmor's custom headers are end-to-end headers.
footnoteref:[header-types, HTTP/1.1 standard ({RFC-7230}#section-6.1[RFC 7230])
defines two types of headers: end-to-end and hop-by-hop headers. End-to-end
headers must be transmitted to the ultimate recipient of a request or response.
Hop-by-hop headers, on the contrary, are meaningful for a single connection
only.]

All headers specified above must be propagated to the services down the call
chain. The header names and values must remain unchanged.

For example, the values of the custom headers like `X-Device-Type` can affect
the results of queries by using device type information to influence
recommendation results. Besides, the values of the custom headers can influence
the results of the queries (e.g. the device type information influences the
recommendation results).

Sometimes the value of a custom header will be used as part of the entity
in a subsequent request. In such cases, the custom headers must still be
propagated as headers with the subsequent request, despite the duplication of
information.

[#233]
== {MUST} Support `X-correlation-ID`

The _Correlation-ID_ is a generic parameter to be passed through service APIs and
events and written into log files and traces. A consequent usage of the
_Correlation-ID_ facilitates the tracking of call flows through our system and allows
the correlation of service activities initiated by a specific call. This is
extremely helpful for operational troubleshooting and log analysis. 

*Data Definition*

The _Correlation-ID_ must be passed through:

* RESTful API requests via {X-Correlation-ID} custom header (see <<184>>)
* Published events via `Correlation_id` event field (see <<event-metadata, metadata>>)

It must be an random unique string consisting of maximal 128 chars, restricted
to the character set `[a-zA-Z0-9/+]` (Base64).

*Note:* If a legacy subsystem can only process _Correlation-IDs_ with a specific
format or length, it must define this restrictions in its API specification,
and be generous and remove invalid characters or cut the length to the
supported limit.

*Service Guidance*

* Services *must* support _Correlation-ID_ as generic input, i.e.
** RESTful API endpoints *must* support {X-Correlation-ID} header in requests
** Event listeners *must* support the metadata `Correlation-id` from events.

+
*Note:*  API-Clients *must* provide _Correlation-ID_ when calling a service or
producing events. If no _CorrelationID_ is provided in a request or event, the
service must create a new _Correlation-ID_.

* Services *must* propagate _Correlation-ID_, i.e. use _Correlation-ID_ received
with API-Calls or consumed events as...
** input for all API called and events published during processing
** data field written for logging and tracing
