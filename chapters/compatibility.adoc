[[compatibility]]
= Compatibility

[#106]
== {MUST} Don’t Break Backward Compatibility

Change APIs, but keep all consumers running. Consumers usually have independent
release lifecycles, focus on stability, and avoid changes that do not provide
additional value. APIs are contracts between service providers and service
consumers that cannot be broken via unilateral decisions.

There are two techniques to change APIs without breaking them:

* follow rules for compatible extensions
* introduce new API versions and still support older versions

We strongly encourage using compatible API extensions and discourage versioning
(see <<113>> and <<114>> below). The following guidelines for service providers
(<<107>>) and consumers (<<108>>) enable us (having Postel’s Law in mind) to
make compatible changes without versioning.

*Note:* There is a difference between incompatible and breaking changes.
Incompatible changes are changes that are not covered by the compatibility
rules below. Breaking changes are incompatible changes deployed into operation,
and thereby breaking running API consumers. Usually, incompatible changes are
breaking changes when deployed into operation. However, in specific controlled
situations it is possible to deploy incompatible changes in a non-breaking way,
if no API consumer is using the affected API aspects (see also <<deprecation,
Deprecation>> guidelines).

*Hint:* Please note that the compatibility guarantees are for the "on the wire"
format. Binary or source compatibility of code generated from an API definition
is not covered by these rules. If client implementations update their
generation process to a new version of the API definition, it has to be
expected that code changes are necessary.


[#107]
== {SHOULD} Prefer Compatible Extensions

API designers should apply the following rules to evolve RESTful APIs for
services in a backward-compatible way:

* Add only optional, never mandatory fields.
* Never change the semantic of fields (e.g. changing the semantic from
  customer-number to customer-id, as both are different unique customer keys)
* Input fields may have (complex) constraints being validated via server-side
  business logic. Never change the validation logic to be more restrictive and
  make sure that all constraints are clearly defined in description.
* Enum ranges can be reduced when used as input parameters, only if the server
  is ready to accept and handle old range values too. Enum range can be reduced
  when used as output parameters.
* Enum ranges cannot be extended when used for output parameters — clients may
  not be prepared to handle it. However, enum ranges can be extended when used
  for input parameters.
* Use {x-extensible-enum}, if range is used for output parameters and likely to
  be extended with growing functionality. It defines an open list of explicit
  values and clients must be agnostic to new values.
* Support redirection in case an URL has to change {301} (Moved Permanently).


[#108]
== {MUST} Prepare Clients To Not Crash On Compatible API Extensions

Service clients should apply the robustness principle:

* Be conservative with API requests and data passed as input, e.g. avoid to
  exploit definition deficits like passing megabytes of strings with
  unspecified maximum length.
* Be tolerant in processing and reading data of API responses, more
  specifically...

Service clients must be prepared for compatible API extensions of service
providers:

* Be tolerant with unknown fields in the payload (see also Fowler’s
  http://martinfowler.com/bliki/TolerantReader.html["TolerantReader"] post),
  i.e. ignore new fields but do not eliminate them from payload if needed for
  subsequent {PUT} requests.
* Be prepared that {x-extensible-enum} return parameter may deliver new values;
  either be agnostic or provide default behavior for unknown values.
* Be prepared to handle HTTP status codes not explicitly specified in endpoint
  definitions. Note also, that status codes are extensible. Default handling is
  how you would treat the corresponding {x00} code (see
  {RFC-7231}#section-6[RFC 7231 Section 6]).
* Follow the redirect when the server returns HTTP status code {301} (Moved
  Permanently).


[#109]
== {SHOULD} Design APIs Conservatively

Designers of service provider APIs should be conservative and accurate in what
they accept from clients:

* Unknown input fields in payload or URL should not be ignored; servers should
  provide error feedback to clients via an HTTP 400 response code.
* Be accurate in defining input data constraints (like formats, ranges, lengths
  etc.) — and check constraints and return dedicated error information in case
  of violations.
* Prefer being more specific and restrictive (if compliant to functional
  requirements), e.g. by defining length range of strings. It may simplify
  implementation while providing freedom for further evolution as compatible
  extensions.

Not ignoring unknown input fields is a specific deviation from Postel's Law
(e.g. see also +
https://cacm.acm.org/magazines/2011/8/114933-the-robustness-principle-reconsidered/fulltext[The
Robustness Principle Reconsidered]) and a strong recommendation. Servers might
want to take different approach but should be aware of the following problems
and be explicit in what is supported:

* Ignoring unknown input fields is actually not an option for {PUT}, since it
  becomes asymmetric with subsequent {GET} response and HTTP is clear about the
  {PUT} _replace_ semantics and default roundtrip expectations (see
  {RFC-7231}#section-4.3.4[RFC 7231 Section 4.3.4]). Note, accepting (i.e. not
  ignoring) unknown input fields and returning it in subsequent {GET} responses
  is a different situation and compliant to {PUT} semantics.
* Certain client errors cannot be recognized by servers, e.g. attribute name
  typing errors will be ignored without server error feedback. The server
  cannot differentiate between the client intentionally providing an additional
  field versus the client sending a mistakenly named field, when the client's
  actual intent was to provide an optional input field.
* Future extensions of the input data structure might be in conflict with
  already ignored fields and, hence, will not be compatible, i.e. break clients
  that already use this field but with different type.

In specific situations, where a (known) input field is not needed anymore, it
either can stay in the API definition with "not used anymore" description or
can be removed from the API definition as long as the server ignores this
specific parameter.


[#110]
== {MUST} Always Return JSON Objects As Top-Level Data Structures To Support Extensibility

In a response body, you must always return a JSON object (and not e.g. an
array) as a top level data structure to support future extensibility. JSON
objects support compatible extension by additional attributes. This allows you
to easily extend your response and e.g. add pagination later, without breaking
backwards compatibility.

Maps (see <<216>>), even though technically objects, are also forbidden as top
level data structures, since they don't support compatible, future extensions.

[#111]
== {MUST} Treat Open API Definitions As Open For Extension By Default

The Open API 2.0 specification is not very specific on default
extensibility of objects, and redefines JSON-Schema keywords related to
extensibility, like `additionalProperties`. Following our overall
compatibility guidelines, Open API object definitions are considered
open for extension by default as per
http://json-schema.org/latest/json-schema-validation.html#rfc.section.5.18[Section
5.18 "additionalProperties"] of JSON-Schema.

When it comes to Open API 2.0, this means an `additionalProperties` declaration
is not required to make an object definition extensible:

* API clients consuming data must not assume that objects are closed for
  extension in the absence of an `additionalProperties` declaration and must
  ignore fields sent by the server they cannot process. This allows API
  servers to evolve their data formats.
* For API servers receiving unexpected data, the situation is slightly
  different. Instead of ignoring fields, servers _may_ reject requests whose
  entities contain undefined fields in order to signal to clients that those
  fields would not be stored on behalf of the client. API designers must
  document clearly how unexpected fields are handled for {PUT}, {POST}, and
  {PATCH} requests.

API formats must not declare `additionalProperties` to be false, as this
prevents objects being extended in the future.

Note that this guideline concentrates on default extensibility and does not
exclude the use of `additionalProperties` with a schema as a value, which might
be appropriate in some circumstances, e.g. see <<216>>.


[#112]
== {SHOULD} Used Open-Ended List of Values (`x-extensible-enum`) Instead of Enumerations

Enumerations are per definition closed sets of values, that are assumed to be
complete and not intended for extension. This closed principle of enumerations
imposes compatibility issues when an enumeration must be extended. To avoid
these issues, we strongly recommend to use an open-ended list of values instead
of an enumeration unless:

1. the API has full control of the enumeration values, i.e. the list of values
  does not depend on any external tool or interface, and
2. the list of value is complete with respect to any thinkable and unthinkable
  future feature.

To specify an open-ended list of values use the marker {x-extensible-enum} as
follows:

[source,yaml]
----
deliver_methods:
  type: string
  x-extensible-enum:
    - parcel
    - letter
    - email
----

*Note:* {x-extensible-enum} is not JSON Schema conform but will be ignored by
most tools.

[#113]
== {SHOULD} Avoid Versioning

When changing your RESTful APIs, do so in a compatible way and avoid generating
additional API versions. Multiple versions can significantly complicate
understanding, testing, maintaining, evolving, operating and releasing our
systems
(http://martinfowler.com/articles/enterpriseREST.html[supplementary
reading]).

If changing an API can’t be done in a compatible way, then proceed in one of
these three ways:

* create a new resource (variant) in addition to the old resource variant
* create a new service endpoint — i.e. a new application with a new API (with a
  new domain name)
* create a new API version supported in parallel with the old API by the same
  microservice

As we discourage versioning by all means because of the manifold disadvantages,
we strongly recommend to only use the first two approaches.


[#114]
== {MUST} Use Global URI Versioning

Although there is no right way as each style come with tradeoffs,
Root Namespace URI versioning, eg `/v1/subscriptions` is the most comment in the
industry and therefore, easiest for a consumer to recognize and use.

Further reading: (https://apisyouwonthate.com/blog/api-versioning-has-no-right-way[API Versioning Has No "Right Way”])
provides an overview on different versioning approaches to handle breaking
changes without being opinionated

[#115]
== {MUST} Use API Lifecycle States

The following is a list of approved lifecycle states.

|===
|State |Description

|PLANNED
|API has been scheduled for development. API release plans have been established.
|Beta
|API is operational and is available to selected new subscribers in production for the purposes of testing, validating, and rolling out a new API.
|LIVE
|API is operational and is available to new subscribers in production. API version is fully supported.
|DEPRECATED
|API is operational and available at runtime to existing subscribers. API version is fully supported, including bug fixes addressed in backwards compatible way. API version is not available to new subscribers.
|RETIRED
|API is unpublished from production and no longer available at runtime to any subscribers. The footprint of all deployed applications entering this state must be completely removed from production and stage environments.
|===

For a given functionality set, there MUST be only one API version in the LIVE 
state at any given time across all major and minor versions. This ensures that
subscribers always understand which versioned API product they should be using.
For example, v1.2 `RETIRED`, v1.3 `DEPRECATED`, or v2.0 `LIVE`.


[#240]
== {MUST} Use API Versioning Policy


API’s are versioned products and MUST adhere to the following versioning
principles.

When versioning, always conform to Semantic Versioning 2.0.0 which use a simple
set of rules and requirements that dictate how version numbers are assigned and
incremented. See <<116>>

Given a version number MAJOR,.MINOR.PATCH, increment the

* MAJOR version when you make incompatible API changes
* MINOR version when you add functionality in a backwards compatible manner
* PATCH version when you make backwards compatible bug fixes.


 
